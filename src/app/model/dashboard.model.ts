export interface UserDetails {
    id: number;
    name: string;
    phone: number;
    email: string;
    username: string;
    website: string;
}