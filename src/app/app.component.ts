import { Component, OnInit } from '@angular/core';
import { DashboardService } from './service/dashboard.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'practise';

  constructor(
    private dashboardService: DashboardService
  ) {

  }
  ngOnInit () {
    // console.log(this.dashboardService.color)
    this.dashboardService.behavior$.subscribe((value: any) => {
      console.log(value);
    })
  }

  updateMe(color: string) {
    this.dashboardService.behavior$.next(color);
  }
}
