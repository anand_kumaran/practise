import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDetails } from '../model/dashboard.model';
import { DashboardService } from '../service/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  behaviorSupscription: Subscription;

  uniqueUserId: number;

  userList: Array<UserDetails> = [];
  

  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    // console.log(this.dashboardService.color)

    this.behaviorSupscription = this.dashboardService.behavior$.subscribe((value: any) => {
      console.log(value);
    })

    this.getAllUserDetails();

    
  }


  getAllUserDetails() {
    this.dashboardService.getUserList()
    .subscribe((response: any) => { 
      if (response && response.length) {
        this.userList = response;
        console.log(this.userList)

        // this.userList = this.userList.filter((user: any) => user.name.includes('in'));

        this.uniqueUserId = this.userList[0].id;
      }

    })
  }




  updateMe(color: string) {
    this.dashboardService.behavior$.next(color);
  }
  
  ngOnDestroy() {
    console.log('destroy');
    this.behaviorSupscription && this.behaviorSupscription.unsubscribe();
  }
}
