import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AsyncSubject, BehaviorSubject, Observable, of, ReplaySubject, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  color = 'orange';

  behavior$ = new BehaviorSubject('default');

  // subject$ = new Subject();
  // replay$ = new ReplaySubject();
  // async$ = new AsyncSubject();

  constructor(
    private http: HttpClient
  ) { } 
  
  getUserList(): Observable<any> {
    return this.http.get('https://jsonplaceholder.typicode.com/users').pipe(
      map((res: Response) => res),
      map(body => body),
      catchError(body => of(body))
    );
  }
}
